﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4RPPOON
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
