﻿using System;

namespace LV4RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            Dataset data = new Dataset("CSV.csv");
            
            Analyzer3rdParty analyze = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyze);
            double[] AverageOfRow = adapter.CalculateAveragePerRow(data);
            foreach (double Number in AverageOfRow)
            {
                Console.WriteLine(Number);
            }
            
            Console.WriteLine("\n");
            
            double[] AverageOfColumn = adapter.CalculateAveragePerColumn(data);
            foreach (double Number in AverageOfColumn)
            {
                Console.WriteLine(Number);
            }
        }
    }
}
